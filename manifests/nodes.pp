node basenode {
  include config
  include ntp
  include java
}

node 'hadoop-01' inherits basenode {
}

node 'hadoop-02' inherits basenode {
}

node 'hadoop-03' inherits basenode {
}

node 'hadoop-04' inherits basenode {
  include hadoop
}

node 'hadoopmaster' inherits basenode {
}
