class config {
	file { "/etc/hosts":
		owner => root,
		group => root,
		mode  => 775,
		source => "puppet:///config/etc/hosts"
	}

	file { "/etc/puppet/auth.conf":
		owner => root,
		group => root,
		mode  => 755,
		source => "puppet:///config/puppet-agent/auth.conf"
	}

	file { "/etc/puppet/puppet.conf":
		owner => root,
		group => root,
		mode  => 755,
		source => "puppet:///config/puppet-agent/puppet.conf"
	}
}

