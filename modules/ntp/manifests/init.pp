class ntp {
	file { '/etc/ntp.conf':
		owner => root,
		group => root,
		mode  => '0644',
		source => "puppet:///ntp/ntp.conf",
		notify => Service['ntp'],
		require => Package['ntp'],
	}

	package { 'ntp':
		ensure => present,
	}

	service { 'ntp':
		ensure     => running,
		enable     => true,
		hasrestart => true,
		hasstatus  => true,
		require    => [
			File['/etc/ntp.conf'],
			Package['ntp']
		],
	}
}

